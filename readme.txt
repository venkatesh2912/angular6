Change Log for angular 6 to angular 5
1) Forms
   -> previous ng-model
        <input [(ngModel)]="name" (ngModelChange)="onChange($event)">
        onChange(value) {
            console.log(value);               // would log updated value
        }

    -> new ng-model
        <input #modelDir="ngModel" [(ngModel)]="name" (ngModelChange)="onChange(modelDir)">
        onChange(ngModel: NgModel) {
            console.log(ngModel.value);        // would log old value, not updated value
        }
        onChange(ngModel: NgModel) {
            console.log(ngModel.value);       // will log updated value
        }

2) ng-update
    ng update uses npm or yarn under the hood to manage dependencies. 
    In addition to updating dependencies and peer dependencies, 
    ng update will apply needed transforms to your project.

3) ng-add
    This new CLI command promises to make adding new capabilities to your project easy. 
    It uses your package manager to “download new dependencies and invoke an installation script 
    (implemented as a schematic) which can update your project with configuration changes, 
    add additional dependencies (e.g. polyfills), or scaffold package-specific initialization code.” 

4) compiler-cli: lower loadChildren fields to allow dynamic module paths
5) <template> tag is replace by <ng-template>
6) RXJS updated to version 6
    Rxjs6 has major changes in this version but you can run old version by using ng-compat.
    or if you want to change it to new code this are the following changes
    ->  Observable, Subject
        Old Code
            import { Observable } from 'rxjs/Observable';
            import { Subject } from 'rxjs/Subject';
        New Code
            import { Observable, Subject } from 'rxjs';
    ->  Operators
        Old Code
            import 'rxjs/add/operator/map';
            import 'rxjs/add/operator/take';
        New Code
            import { map, take } from 'rxjs/operators'; 
    ->  Create Observables
        Old Code
            import 'rxjs/add/observable/of';
            // or 
            import { of } from 'rxjs/observable/of';
        New Code
            import { of } from 'rxjs';   
    ->  Map Operator
        Old Code
            import 'rxjs/add/operator/map'
            myObservable
            .map(data => data * 2)
            .subscribe(...);
        New Code
            import { map } from 'rxjs/operators';
    -> Newly Introduces pipe() method           
        pipe takes an infinite amount of arguments and each argument is an operator you want
        to apply to the Observable.
        Ex 1:
            import { map } from 'rxjs/operators';
            myObservable
                .pipe(map(data => data * 2))
                .subscribe(...);
        Ex 2: You can also make chain in the pipe like 
            import { map, switchMap, throttle } from 'rxjs/operators';
            myObservable
            .pipe(map(data => data * 2), switchMap(...), throttle(...))
            .subscribe(...);





